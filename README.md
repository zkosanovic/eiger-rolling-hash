# eiger-rolling-hash

Rolling hash based file diffing.

This CLI tool will show us "delta" of two files - chunks that we can re-use from the first file and literal data we have to write to the second file to sync it with the first one.

## Requirements

- Go 1.17+

## Usage

```bash
go build
./eiger-rolling-hash diff testfiles/1.txt testfiles/2.txt
```