// Package checksum provides functions to compute checksums on the given byte blocks.
//
// It implements the variation of Adler-32 checksum algorithm used by rsync utility.
// The algorithm is implemented as described here: https://rsync.samba.org/tech_report/node3.html
package checksum

import (
	"errors"
	"fmt"
	"strings"
)

const mod = 65521

type Checksum struct {
	block []byte
	a     uint16
	b     uint16
}

// Sum returns the final checksum.
func (c Checksum) Sum() uint32 {
	return uint32(c.a) + uint32(c.b)<<16
}

// String returns string representation of a Checksum.
func (c Checksum) String() string {
	builder := strings.Builder{}
	builder.WriteString("[ ")
	for _, b := range c.block {
		builder.WriteString(fmt.Sprintf("%d ", b))
	}
	builder.WriteRune(']')
	return fmt.Sprintf("{%s %v %v}", builder.String(), c.a, c.b)
}

// New computes a new Checksum from a given block of bytes.
func New(block []byte) Checksum {
	var a, b uint16
	l := len(block)
	for i, bt := range block {
		a = (a + uint16(bt)) % mod
		b = (b + uint16(l-i)*uint16(bt)) % mod
	}
	return Checksum{
		block: block,
		a:     a,
		b:     b,
	}
}

// Roll computes the next checksum by "rolling" the previously computed one.
func Roll(prev Checksum, next byte) (Checksum, error) {
	l := byte(len(prev.block))
	if l == 0 {
		return Checksum{}, errors.New("previous checksum block empty")
	}
	a := (prev.a - uint16(prev.block[0]) + uint16(next)) % mod
	b := (prev.b - uint16(l)*uint16(prev.block[0]) + a) % mod

	return Checksum{
		block: append(prev.block[1:], next),
		a:     a,
		b:     b,
	}, nil
}
