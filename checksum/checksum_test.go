package checksum

import (
	"errors"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNew(t *testing.T) {
	tests := []struct {
		block    []byte
		expected Checksum
	}{
		{
			[]byte{1, 2, 3, 4, 5, 6, 7, 8},
			Checksum{
				block: []byte{1, 2, 3, 4, 5, 6, 7, 8},
				a: 36,
				b: 120,
			},
		},
	}

	for _, tc := range tests {
		actual := New(tc.block)
		if diff := cmp.Diff(tc.expected, actual, cmp.AllowUnexported(Checksum{})); diff != "" {
			t.Errorf("checksum not as expected: %v", diff)
		}
	}
}

func TestSum(t *testing.T) {
	tests := []struct {
		checksum Checksum
		expected uint32
	}{
		{
			Checksum{
				a:     0,
				b:     0,
			},
			0,
		},
		{
			Checksum{
				a:     125,
				b:     125,
			},
			8192125,
		},
		{
			Checksum{
				a:     0,
				b:     1,
			},
			1 << 16,
		},
		{
			Checksum{
				a:     1000,
				b:     0,
			},
			1000,
		},
	}

	for _, tc := range tests {
		actual := tc.checksum.Sum()
		if actual != tc.expected {
			t.Errorf("sum not as expected, want: %v; got: %v", tc.expected, actual)
		}
	}
}

func TestRoll(t *testing.T) {
	tests := []struct {
		prev Checksum
		next byte
		expected Checksum
		expectedErr error
	}{
		{
			Checksum{
				block: []byte{},
				a:     0,
				b:     0,
			},
			23,
			Checksum{},
			errors.New("previous checksum block empty"),
		},
		{
			Checksum{
				block: []byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
				a:     136,
				b:     816,
			},
			17,
			Checksum{
				block: []byte{2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17},
				a: 152,
				b: 952,
			},
			nil,
		},
		{
			Checksum{
				block: []byte{10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160},
				a:     1360,
				b:     8160,
			},
			170,
			New([]byte{20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170}),
			nil,
		},
		{
			New([]byte("Lorem ipsum dolo")),
			'r',
			New([]byte("orem ipsum dolor")),
			nil,
		},
	}

	for _, tc := range tests {
		actual, err := Roll(tc.prev, tc.next)
		if diff := cmp.Diff(tc.expected, actual, cmp.AllowUnexported(Checksum{})); diff != "" {
			t.Errorf("checksum not as expected: %v", diff)
		}
		if tc.expectedErr != nil && (err == nil || err.Error() != tc.expectedErr.Error()) {
			t.Errorf("error not as expected, want: %v; got: %v", tc.expectedErr, err)
		}
		if tc.expectedErr == nil && err != nil {
			t.Errorf("unexpected error: %v", err)
		}
	}
}