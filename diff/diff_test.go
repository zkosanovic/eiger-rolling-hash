package diff

import (
	"errors"
	"io"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestBuildTable(t *testing.T) {
	tests := []struct {
		reader      io.Reader
		expected    Table
		expectedErr error
	}{
		{
			strings.NewReader("abcdefghijklm"),
			nil,
			errors.New("unsufficient data"),
		},
		{
			strings.NewReader("abcdefghijklm1234"),
			Table{
				885130705: {
					"fb4c7211d8ad4c77f0f2679736900c8d": 0,
				},
				978716068: {
					"48a58f2bbc4f5dca13c1fcae1b40b5e8": 1,
				},
			},
			nil,
		},
	}

	for _, tc := range tests {
		diff := New(tc.reader)
		table, err := diff.BuildTable()
		if diff := cmp.Diff(tc.expected, table); diff != "" {
			t.Errorf("table not as expected: %v", diff)
		}
		if tc.expectedErr != nil && (err == nil || err.Error() != tc.expectedErr.Error()) {
			t.Errorf("error not as expected, want: %v; got: %v", tc.expectedErr, err)
		}
		if tc.expectedErr == nil && err != nil {
			t.Errorf("unexpected error: %v", err)
		}
	}
}
