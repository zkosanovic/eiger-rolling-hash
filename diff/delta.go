package diff

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"io"

	"eiger-rolling-hash/checksum"
)

// Chunk represents a chunk of data that can be either:
// 1. Defined by Position in the original file (if Existing = true).
// 2. Defined by literal data stored in Data slice (if Existing = false).
type Chunk struct {
	Existing bool
	Data []byte
	Position uint
}

func (c Chunk) String() string {
	if c.Existing {
		return fmt.Sprintf("[pos: %d]", c.Position)
	}
	return fmt.Sprintf("[data: %s]", string(c.Data))
}

func (d Diff) Delta(r io.Reader) ([]Chunk, error) {
	table, err := d.BuildTable()
	if err != nil {
		return nil, err
	}

	block := make([]byte, blockSize)

	n, err := r.Read(block)
	if n < blockSize || err == io.EOF {
		return nil, errors.New("unsufficient data")
	}
	if err != nil {
		return nil, fmt.Errorf("couldn't read bytes: %w", err)
	}

	weak := checksum.New(block)
	chunks := make([]Chunk, 0)
	literalBuffer := make([]byte, 0, blockSize)
	buff := make([]byte, 1)

	for {
		if h, ok := table[weak.Sum()]; ok {
			hash := md5.Sum(block)
			strong := hex.EncodeToString(hash[:])
			if pos, ok := h[strong]; ok {
				if len(literalBuffer) > 0 {
					data := make([]byte, len(literalBuffer))
					copy(data, literalBuffer)
					chunks = append(chunks, Chunk{
						Data: data,
					})
					literalBuffer = make([]byte, 0, blockSize)
				}
				chunks = append(chunks, Chunk{
					Existing: true,
					Position: pos,
				})

				n, err = r.Read(block)
				if n < blockSize || err == io.EOF {
					literalBuffer = append(literalBuffer, block[:n]...)
					chunks = append(chunks, Chunk{
						Data: literalBuffer,
					})

					break
				}
				if err != nil {
					return nil, fmt.Errorf("couldn't read bytes: %w", err)
				}
				weak = checksum.New(block)
			} else {
				literalBuffer = append(literalBuffer, block[0])
			}
		} else {
			literalBuffer = append(literalBuffer, block[0])
		}

		n, err = r.Read(buff)
		if n == 0 && err == io.EOF {
			literalBuffer = append(literalBuffer, block[1:]...)
			chunks = append(chunks, Chunk{
				Data: literalBuffer,
			})

			break
		}
		if n == 0 && err != nil {
			return nil, fmt.Errorf("couldn't read next byte: %w", err)
		}

		block = append(block[1:], buff[0])

		weak, err = checksum.Roll(weak, buff[0])
		if err != nil {
			return nil, err
		}
	}

	return chunks, nil
}
