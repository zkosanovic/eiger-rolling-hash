package diff

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"io"

	"eiger-rolling-hash/checksum"
)

const blockSize = 16

type Table map[uint32]map[string]uint

type Diff struct {
	reader io.Reader
}

func New(r io.Reader) Diff {
	return Diff{
		reader: r,
	}
}

// BuildTable builds a Table map that maps the byte offset using
// the weak (Adler-32 inspired) and the strong (MD5) checksum.
func (d Diff) BuildTable() (Table, error) {
	table := make(Table)
	block := make([]byte, blockSize)

	n, err := d.reader.Read(block)
	if n < blockSize || err == io.EOF {
		return nil, errors.New("unsufficient data")
	}
	if err != nil {
		return nil, fmt.Errorf("couldn't read bytes: %w", err)
	}

	pos := uint(0)
	weak := checksum.New(block)
	hash := md5.Sum(block)
	strong := hex.EncodeToString(hash[:])

	if table[weak.Sum()] == nil {
		table[weak.Sum()] = make(map[string]uint)
	}
	table[weak.Sum()][strong] = pos

	buff := make([]byte, 1)
	for {
		n, err := d.reader.Read(buff)
		if n == 0 && err == io.EOF {
			break
		}
		if n == 0 && err != nil {
			return nil, fmt.Errorf("couldn't read next byte: %w", err)
		}

		pos += 1
		weak, err = checksum.Roll(weak, buff[0])
		if err != nil {
			return nil, err
		}
		block = append(block[1:], buff[0])
		hash = md5.Sum(block)
		strong = hex.EncodeToString(hash[:])

		if table[weak.Sum()] == nil {
			table[weak.Sum()] = make(map[string]uint)
		}
		table[weak.Sum()][strong] = pos
	}

	return table, nil
}
