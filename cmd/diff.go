package cmd

import (
	"fmt"
	"os"

	"eiger-rolling-hash/diff"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(diffCmd)
}

var diffCmd = &cobra.Command{
	Use:   "diff",
	Args: cobra.MatchAll(cobra.ExactArgs(2), cobra.OnlyValidArgs),
	Short: "Rolling hash based file diffing",
	Long:  `Prints the difference between two files.
Accepts the paths to the two files as arguments.`,
	Run: runDiff,
}

func runDiff(_ *cobra.Command, args []string) {
	paths := args[:2]

	file1, err := os.Open(paths[0])
	cobra.CheckErr(err)
	file2, err := os.Open(paths[1])
	cobra.CheckErr(err)

	chunks, err := diff.New(file1).Delta(file2)
	cobra.CheckErr(err)

	fmt.Println(chunks)
}