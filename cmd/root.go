package cmd

import (
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "eiger-rolling-hash",
	Short: "A utility that can be used to show diff between two files",
	Long: `eiger-rolling-hash is a Go library that can be used to show diff between two files.
It uses rolling-hash algorithm to check the changes between the files.`,
}

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

